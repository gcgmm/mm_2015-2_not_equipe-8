Instruções para abrir o projeto:

1) Tenha o Unity 5.2.3f1 ou maior instalado
2) Abra o arquivo cena_Teste.unity localizado em Polimetry_SourceCode\Assets\Polimetry

Instruções para executar o Programa:

1) Execute o arquivo Polimetry.exe localizado na pasta Polimetry_Exe

Controles:

Segure o botão direito ou as teclas WASD para mover a camera
Use o Scroll Wheel do Mouse para aproximar/afastar a camera
Use a Tecla F para entrar no modo "Espaço"
Use a Tecla M para desmutar a musica