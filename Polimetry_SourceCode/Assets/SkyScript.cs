﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SkyScript : MonoBehaviour {

    public GameObject menuEsquerdo, menuDireito, menuFormas;
	private bool sky = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void moveMenus(){
		sky = !sky;
		menuEsquerdo.SetActive(sky);
		menuDireito.SetActive(sky);
		menuFormas.SetActive(sky);

	}
}
