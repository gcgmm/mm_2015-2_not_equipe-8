﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeText : MonoBehaviour {
	Text texto;

	private float valor = 1f;

	public string textoInicial = "1";
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Text>().text = textoInicial;
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<Text>().text = valor.ToString();
	}

	public void AlteraValor(float value){
		valor = value;
	}

}
