using UnityEngine;
using System.Collections;

public class ParalelepipedoPlanificado :
	PrismaPlanificado
{	


	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro Planificado");
		ret.AddComponent<ParalelepipedoMontado>().quantVerticesBase = quantVerticesBase;
		return ret;
	}
}
