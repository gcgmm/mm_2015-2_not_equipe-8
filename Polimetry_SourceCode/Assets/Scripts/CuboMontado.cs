﻿using UnityEngine;
using System.Collections;

public class CuboMontado :
	PrismaMontado
{

	/*

	protected void montar ()
	{
		Vector3 posicaoAtual = gameObject.transform.position;
		float deslocamento = this.valorA / 2.0f;




		Vector3[] vertices = new Vector3[8]
		{
			new Vector3 (deslocamento, -deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, -deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, -deslocamento, -deslocamento) + posicaoAtual,
			new Vector3 (deslocamento, -deslocamento, -deslocamento) + posicaoAtual,
			new Vector3 (deslocamento, deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, deslocamento, -deslocamento) + posicaoAtual,
			new Vector3 (deslocamento, deslocamento, -deslocamento) + posicaoAtual
		};



		transform.position = new Vector3 (0.0f, 0.25f * valorA, 0.0f);


		int[] triangulos = new int[36]
		{
			5,1,0,
			4,5,0,
			
			6,2,1,
			5,6,1,
			
			7,3,2,
			6,7,2,
			
			4,0,3,
			7,4,3,
			
			0,1,2,
			0,2,3,
			
			7,6,4,
			6,5,4
			
		};
		updateMesh(vertices,triangulos);

	}
*/
	override public void Start ()
	{
		base.Start();
		quantVerticesBase = 4;
		gameObject.transform.Rotate(0.0f, 45.0f, 0.0f);

	}

	void Update ()
	{
		medidaB = medidaA;

	}

	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro Planificado");
		ret.AddComponent<CuboPlanificado>().quantVerticesBase = quantVerticesBase;
		return ret;
	}
}
