﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlePoliedros : MonoBehaviour
{
	public Slider sliderA;
	public Slider sliderB;
	public Slider sliderC;
	public Material mat;
	// Use this for initialization
	private GameObject poliedro;

	void Start ()
	{


	}

	// Update is called once per frame
	void Update ()
	{
		if (poliedro != null && poliedro.GetComponent<Poliedro> () != null) {
			poliedro.GetComponent<Poliedro> ().medidaA = sliderA.value;
			poliedro.GetComponent<Poliedro> ().medidaB = sliderB.value;
			poliedro.GetComponent<Poliedro> ().medidaC = sliderC.value;
		}

		controlePlanificado();

	}
	private void controlePlanificado(){

		if(Input.GetKeyDown(KeyCode.P))
		{
			GameObject novo = poliedro.GetComponent<Poliedro>().planificar();
			Destroy(poliedro);
			poliedro = novo;
			poliedro.transform.parent = this.transform;
			poliedro.AddComponent<MeshFilter>();
			poliedro.AddComponent<MeshRenderer>().material = mat;
			if(poliedro.GetComponent<PiramideMontada>() != null && poliedro.GetComponent<Poliedro>().quantVerticesBase == 4)
				poliedro.transform.Rotate(0f,45f,0f);

		}

	}
	
	protected void buildPoliedro() {
		if(poliedro!=null)
			Destroy(poliedro);
		poliedro = new GameObject("Poliedro");
		poliedro.transform.parent = this.transform;
		poliedro.AddComponent<MeshFilter>();
		poliedro.AddComponent<MeshRenderer>().material = mat;

	}

	public void montarCubo ()
	{
		buildPoliedro();
		poliedro.AddComponent<CuboMontado>();
	}

	public void montarPiramide(int value)
	{
		buildPoliedro();
		poliedro.AddComponent<PiramideMontada>().quantVerticesBase = value;
		if(value==4)
			poliedro.transform.Rotate(0.0f, 45.0f,0.0f);
	}

	public void montarPiramideRetangular()
	{
		buildPoliedro();
		poliedro.AddComponent<PiramideRetangular>().quantVerticesBase = 4;
	}

	public void montarPrisma(int value)
	{
		buildPoliedro();
		poliedro.AddComponent<PrismaMontado>().quantVerticesBase = value;

	}
	public void montarParalelepipedo(){
		buildPoliedro();
		poliedro.AddComponent<ParalelepipedoMontado>();
	
	}


}
