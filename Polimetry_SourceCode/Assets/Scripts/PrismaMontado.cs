﻿using UnityEngine;
using System.Collections;


public class PrismaMontado :
	Poliedro
{

	public float getLado  () { return getMedidaA (); }
	public float getAltura() { return getMedidaB (); }


	public PrismaMontado() : base() { }

	override protected void montar()
	{
		Vector3 posicaoCentral = gameObject.transform.position;
		int
			numVertBase   = getQuantVerticesBase (),
			numVertTotal  = 2 * getQuantVerticesBase(),
			numTriangBase = getQuantVerticesBase() - 2;
		float
			deslocVertical = getAltura() / 2.0f,
			raio           = getRaioExternoBase(getLado()),
			anguloInterno  = getAnguloInternoRad(),
			anguloVertice  = 0.0f;


		Vector3[] vertices   = new Vector3[numVertTotal];
		int[]     triangulos = new int    [12 * (numVertBase - 1)];

		int
			primeiroVertice = 0,
			ultimoVertice   = numVertTotal - 1;


		for (int v = 0; v < numVertBase; ++v)
		{
			int
				vertBase = primeiroVertice + v,
				vertTopo = numVertBase     + v;
			float
				deslocX = raio * Mathf.Cos(anguloVertice),
				deslocY = raio * Mathf.Sin(anguloVertice);


			//Face base.
			vertices[vertBase].x = posicaoCentral.x + deslocX;
			vertices[vertBase].z = posicaoCentral.z + deslocY;

			vertices[vertBase].y = posicaoCentral.y - deslocVertical;


			//Face topo.
			vertices[vertTopo].x = posicaoCentral.x + deslocX;
			vertices[vertTopo].z = posicaoCentral.z + deslocY;
			
			vertices[vertTopo].y = posicaoCentral.y + deslocVertical;


			anguloVertice += anguloInterno;

		}


		for (int t = 0; t < numTriangBase; ++t)
		{
			int
				posTriangBase = 3 * t,
				posTriangTopo = triangulos.Length - 3 * (numTriangBase - t);


			//Face base.
			triangulos[posTriangBase]     = primeiroVertice;
			triangulos[posTriangBase + 1] = primeiroVertice + (t + 1);
			triangulos[posTriangBase + 2] = primeiroVertice + (t + 2);


			//Face topo
			triangulos[posTriangTopo]     = ultimoVertice;
			triangulos[posTriangTopo + 1] = ultimoVertice - (t + 1);
			triangulos[posTriangTopo + 2] = ultimoVertice - (t + 2);
		}


		int primeiraPosTriangLaterais = 3 * numTriangBase;

		for (int f = 0; f < numVertBase; ++f)
		{
			int posTriang = primeiraPosTriangLaterais + 6 * f;

			//Face lateral.
			triangulos[posTriang]     = primeiroVertice + f;
			triangulos[posTriang + 1] = numVertBase + f;
			triangulos[posTriang + 2] = numVertBase + (f + 1) % numVertBase;

			triangulos[posTriang + 3] = primeiroVertice + f;
			triangulos[posTriang + 4] = triangulos[posTriang + 2];
			triangulos[posTriang + 5] = primeiroVertice + (f + 1) % numVertBase;
		}


		updateMesh (vertices,triangulos);
	}

	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro Planificado");
		ret.AddComponent<PrismaPlanificado>().quantVerticesBase = quantVerticesBase;
		return ret;
	}

}
