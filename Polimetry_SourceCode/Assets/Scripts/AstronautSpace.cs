﻿ using UnityEngine;
using System.Collections;

public class AstronautSpace : MonoBehaviour {

	bool sky = false;
	public GameObject go;
	public float speed = 50f;
//	public float distancia = 50f;
	private float x = 0, y = 0, z = 0;
	// Use this for initialization
	void Start () {
		//gameObject.transform.position = new Vector3(0,0,-distancia);
		gameObject.GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F)) {
			sky = !sky;
			gameObject.GetComponent<MeshRenderer>().enabled = sky;
		}
		gameObject.transform.RotateAround(go.transform.position, Vector3.right, (speed*Time.deltaTime));

		x = (int) Random.Range(0, 2);
		y = (int) Random.Range(0, 2);
		z = (int) Random.Range(0, 2);
		gameObject.transform.Rotate(speed*Time.deltaTime*x, speed*Time.deltaTime*y, speed*Time.deltaTime*z);
	}

}
