﻿using UnityEngine;
using System.Collections;

public class ParalelepipedoMontado :
	PrismaMontado
{

	private float getLargura() { return getMedidaA(); }
	private float getComprimento() { return getMedidaC(); }

	/*

	protected void montar ()
	{
		Vector3 posicaoAtual = gameObject.transform.position;
		float deslocamento = this.valorA / 2.0f;




		Vector3[] vertices = new Vector3[8]
		{
			new Vector3 (deslocamento, -deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, -deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, -deslocamento, -deslocamento) + posicaoAtual,
			new Vector3 (deslocamento, -deslocamento, -deslocamento) + posicaoAtual,
			new Vector3 (deslocamento, deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, deslocamento, deslocamento) + posicaoAtual,
			new Vector3 (-deslocamento, deslocamento, -deslocamento) + posicaoAtual,
			new Vector3 (deslocamento, deslocamento, -deslocamento) + posicaoAtual
		};



		transform.position = new Vector3 (0.0f, 0.25f * valorA, 0.0f);


		int[] triangulos = new int[36]
		{
			5,1,0,
			4,5,0,
			
			6,2,1,
			5,6,1,
			
			7,3,2,
			6,7,2,
			
			4,0,3,
			7,4,3,
			
			0,1,2,
			0,2,3,
			
			7,6,4,
			6,5,4
			
		};
		updateMesh(vertices,triangulos);

	}
*/
	override public void Start ()
	{
		base.Start();
		quantVerticesBase = 4;
//		gameObject.transform.Rotate(0.0f, 45.0f, 0.0f);

	}

	override protected void montar()
	{
		Vector3 posicaoCentral = gameObject.transform.position;
		int
			numVertBase   = getQuantVerticesBase (),
			numVertTotal  = 2 * getQuantVerticesBase(),
			numTriangBase = getQuantVerticesBase() - 2;
		//float
		//	deslocVertical = getAltura() / 2.0f,
			//raio           = getRaioBase(),	
			//anguloInterno  = getAnguloInternoRad(),
			//anguloVertice  = 0.0f;
		
		
		Vector3[] vertices   = new Vector3[numVertTotal];
		int[]     triangulos = new int    [12 * (numVertBase - 1)];
		
		int
			primeiroVertice = 0,
			ultimoVertice   = numVertTotal - 1;

		vertices[0].x = posicaoCentral.x + getLargura() / 2;
		vertices[0].y = posicaoCentral.y - getAltura() / 2;
		vertices[0].z = posicaoCentral.z - getComprimento() / 2;

		vertices[1].x = posicaoCentral.x + getLargura() / 2;
		vertices[1].y = posicaoCentral.y - getAltura() / 2;
		vertices[1].z = posicaoCentral.z + getComprimento() / 2;

		vertices[2].x = posicaoCentral.x - getLargura() / 2;
		vertices[2].y = posicaoCentral.y - getAltura() / 2;
		vertices[2].z = posicaoCentral.z + getComprimento() / 2;

		vertices[3].x = posicaoCentral.x - getLargura() / 2;
		vertices[3].y = posicaoCentral.y - getAltura() / 2;
		vertices[3].z = posicaoCentral.z - getComprimento() / 2;

		vertices[4].x = posicaoCentral.x + getLargura() / 2;
		vertices[4].y = posicaoCentral.y + getAltura() / 2;
		vertices[4].z = posicaoCentral.z - getComprimento() / 2;

		vertices[5].x = posicaoCentral.x + getLargura() / 2;
		vertices[5].y = posicaoCentral.y + getAltura() / 2;
		vertices[5].z = posicaoCentral.z + getComprimento() / 2;

		vertices[6].x = posicaoCentral.x - getLargura() / 2;
		vertices[6].y = posicaoCentral.y + getAltura() / 2;
		vertices[6].z = posicaoCentral.z + getComprimento() / 2;

		vertices[7].x = posicaoCentral.x - getLargura() / 2;
		vertices[7].y = posicaoCentral.y + getAltura() / 2;
		vertices[7].z = posicaoCentral.z - getComprimento() / 2;

		
		for (int t = 0; t < numTriangBase; ++t)
		{
			int
				posTriangBase = 3 * t,
				posTriangTopo = triangulos.Length - 3 * (numTriangBase - t);
			
			
			//Face base.
			triangulos[posTriangBase]     = primeiroVertice;
			triangulos[posTriangBase + 1] = primeiroVertice + (t + 1);
			triangulos[posTriangBase + 2] = primeiroVertice + (t + 2);
			
			
			//Face topo
			triangulos[posTriangTopo]     = ultimoVertice;
			triangulos[posTriangTopo + 1] = ultimoVertice - (t + 1);
			triangulos[posTriangTopo + 2] = ultimoVertice - (t + 2);
		}
		
		
		int primeiraPosTriangLaterais = 3 * numTriangBase;
		
		for (int f = 0; f < numVertBase; ++f)
		{
			int posTriang = primeiraPosTriangLaterais + 6 * f;
			
			//Face lateral.
			triangulos[posTriang]     = primeiroVertice + f;
			triangulos[posTriang + 1] = numVertBase + f;
			triangulos[posTriang + 2] = numVertBase + (f + 1) % numVertBase;
			
			triangulos[posTriang + 3] = primeiroVertice + f;
			triangulos[posTriang + 4] = triangulos[posTriang + 2];
			triangulos[posTriang + 5] = primeiroVertice + (f + 1) % numVertBase;
		}
		
		
		updateMesh (vertices,triangulos);
	}

	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro Planificado");
		ret.AddComponent<ParalelepipedoPlanificado>().quantVerticesBase = quantVerticesBase;
		return ret;
	}
}
