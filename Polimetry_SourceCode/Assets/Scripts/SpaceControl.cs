﻿using UnityEngine;
using System.Collections;

public class SpaceControl : MonoBehaviour {

	bool sky = false;
	public GameObject go;
	public float speed = 2f;
	public float distancia = 50f;

	// Use this for initialization
	void Start () {
		gameObject.transform.position = new Vector3(0,0,-distancia);
		gameObject.GetComponent<MeshRenderer>().enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F)) {
			sky = !sky;
			gameObject.GetComponent<MeshRenderer>().enabled = sky;
		}
		gameObject.transform.RotateAround(go.transform.position, Vector3.up, (speed*Time.deltaTime));
	}
}
