﻿using UnityEngine;
using System.Collections;


public class PiramideRetangular :
	Poliedro
{

	public PiramideRetangular() : base() { }


	public float getLargura  () { return getMedidaA (); }
	public float getAltura() { return getMedidaB (); }
	public float getComprimento  () { return getMedidaC (); }


	override protected void montar()
	{
		Vector3 posicaoCentral = gameObject.transform.position;

		int
			numVertBase     = getQuantVerticesBase(),
			numVertTotal    = getQuantVerticesBase() + 1,
			numTriangBase   = getQuantVerticesBase() - 2;
		int
			primeiroVertice      = 0,
			ultimoVertice        = numVertTotal - 1,
			inicioTriangLaterais = 3 * numTriangBase;
		float
			deslocVertical = getAltura() / 2.0f;
			//raio           = getRaioBase(),
			//anguloInterno  = getAnguloInternoRad(),
			//anguloVertice  = 0.0f;

		Vector3[] vertices   = new Vector3[numVertTotal];
		int[]     triangulos = new int    [3 * (numTriangBase + numVertBase)];

		/*
		for (int v = 0; v < numVertBase; ++v)
		{
			vertices[v] = posicaoCentral;

			vertices[v].x += raio * Mathf.Cos(anguloVertice);
			vertices[v].z += raio * Mathf.Sin(anguloVertice);

			vertices[v].y -= deslocVertical;


			anguloVertice += anguloInterno;
		}
		*/
		{

			vertices[0].x = posicaoCentral.x + getLargura() / 2;
			vertices[0].y = posicaoCentral.y - getAltura() / 2;
			vertices[0].z = posicaoCentral.z - getComprimento() / 2;
			
			vertices[1].x = posicaoCentral.x + getLargura() / 2;
			vertices[1].y = posicaoCentral.y - getAltura() / 2;
			vertices[1].z = posicaoCentral.z + getComprimento() / 2;
			
			vertices[2].x = posicaoCentral.x - getLargura() / 2;
			vertices[2].y = posicaoCentral.y - getAltura() / 2;
			vertices[2].z = posicaoCentral.z + getComprimento() / 2;
			
			vertices[3].x = posicaoCentral.x - getLargura() / 2;
			vertices[3].y = posicaoCentral.y - getAltura() / 2;
			vertices[3].z = posicaoCentral.z - getComprimento() / 2;

		}

		{
			vertices[ultimoVertice] = posicaoCentral;

			vertices[ultimoVertice].y += deslocVertical;
		}

		{
			int posTriangBase = 0;

			for (int t = 0; t < numTriangBase; ++t)
			{
				//Face base.
				triangulos[posTriangBase]     = primeiroVertice;
				triangulos[posTriangBase + 1] = primeiroVertice + (t + 1);
				triangulos[posTriangBase + 2] = primeiroVertice + (t + 2);

				posTriangBase += 3;
			}
		}

		{
			int posInicioTriang = inicioTriangLaterais;

			for (int f = 0; f < numVertBase; ++f)
			{
				triangulos[posInicioTriang]     = primeiroVertice + f;
				triangulos[posInicioTriang + 1] = ultimoVertice;
				triangulos[posInicioTriang + 2] = primeiroVertice + (f + 1) % numVertBase;

				posInicioTriang += 3;
			}
		}


		updateMesh (vertices, triangulos);
	}

	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro Planificado");
		ret.AddComponent<PiramidePlanificada>().quantVerticesBase = quantVerticesBase;
		return ret;
	}

}
