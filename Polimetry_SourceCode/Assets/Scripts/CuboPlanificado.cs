using UnityEngine;
using System.Collections;

public class CuboPlanificado :
	PrismaPlanificado
{
	override public void Start ()
	{
		base.Start();		
	}
	
	void Update ()
	{
		medidaB = medidaA;
	}

	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro Planificado");
		ret.AddComponent<CuboMontado>().quantVerticesBase = quantVerticesBase;
		return ret;
	}
}
