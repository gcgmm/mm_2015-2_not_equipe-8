﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlFormulas : MonoBehaviour {

	public Slider sliderA;
	public Slider sliderB;
	public Slider sliderC;

	public Text textArea1;
	public Text textArea2;
    
	public Text textVolume1;
	public Text textVolume2;

	public Text textCarac1;
	public Text textCarac2;

	public Text textAreaF1;
	public Text textAreaF2;

	public Text textVolumeF1;
	public Text textVolumeF2;

	public Text textCaracFull1;
	public Text textCaracFull2;

	public Text titulo1;
	public Text titulo2;



	private int tipoForma = 0;

	// Use this for initialization
	void Start () {
		textArea1.text = textArea2.text = "Área:";
		textVolume1.text = textVolume2.text = "Volume:";
		textCarac1.text = textCarac2.text = "Características:";

		sliderA.value = 1;
    }

	// Update is called once per frame
	void Update () {
		textArea1.text = textArea2.text = "Área: "+ calculaArea();
		textVolume1.text = textVolume2.text = "Volume: " +calculaVolume();
		textCaracFull1.text = textCaracFull2.text = getCaracteristicas();
	}

	private float calculaArea(){
		float apotema = 0;
		switch(tipoForma)
		{
		case(0):
			return 0;
		case(1):
			apotema = Mathf.Sqrt( Mathf.Pow(sliderB.value,2) + Mathf.Pow( sliderA.value/ ( 2 * Mathf.Tan( (Mathf.PI*2)/(8))),2 ));
			return (((sliderA.value / 2) * apotema)*3) + (((sliderA.value*sliderA.value)*Mathf.Sqrt(3))/4);
		case(2):
			apotema = Mathf.Sqrt( Mathf.Pow(sliderB.value,2) + Mathf.Pow( sliderA.value/ ( 2 * Mathf.Tan( (Mathf.PI*2)/(8))),2 ));
			return (((sliderA.value / 2) * apotema)*4) + Mathf.Pow(sliderA.value,2);
        case (3):
			return (sliderA.value*sliderB.value);
		case(4):
			return ((5*sliderA.value*sliderB.value)+(2*((5*Mathf.Pow(sliderA.value,2)*Mathf.Sqrt(3))/4)));
		case(5):
			return ((6*sliderA.value*sliderB.value)+(2*((6*Mathf.Pow(sliderA.value,2)*Mathf.Sqrt(3))/4)));
		case(6):
			return (2*((sliderA.value*sliderB.value)+(sliderB.value*sliderC.value)+(sliderA.value*sliderC.value)));
		case(7):
			return ((sliderA.value*sliderA.value)*6);
		}
		return 0;
	}
	private float calculaVolume(){
		switch(tipoForma)
		{
		case(0):
			return 0;
		case(1):
					return 0;
		case(2):
					return (Mathf.Pow(sliderA.value, 2) * sliderB.value) / 3;
		case(3):
					return (sliderA.value * sliderB.value * sliderB.value)/3;
		case(4):
			return (((5*Mathf.Pow(sliderA.value,2)*Mathf.Sqrt(3))/4)*sliderB.value);
		case(5):
			return (((6*Mathf.Pow(sliderA.value,2)*Mathf.Sqrt(3))/4)*sliderB.value);
		case(6):
			return (sliderA.value*sliderB.value*sliderC.value);
		case(7):
			return ((sliderA.value*sliderA.value)*sliderA.value);
		}
		return 0;
	}
	private string calculaTextoArea(){
		switch(tipoForma)
		{
		case(0):
			return "";
		case(1):
			return "#TODO\n" +
				"((apotema * (a/2))*3) + ((a²*"+"\u221A9"+"3)/4)";
		case(2):
                return "((apotema * (a/2))*4) + a²";
		case(3):
			return "a X b";
		case(4):
			return ("(5 X ab)+(2*((5 X a² X "+"\u221A9"+"3)/4))");
		case(5):
			return ("(6 X ab)+(2 X ((6 X a² X "+"\u221A9"+"3)/4))");
		case(6):
			return ("2 X (ab+bc+ac)");
		case(7):
			return ("a² X 6");
		}
		return "";
	}
	private string calculaTextoVolume(){
		switch(tipoForma){
			case(0):
				return "";
			case(1):
				return "1/3 X (1/2 X ) X b #TODO";
			case(2):
				return "1/3 X a² X b";
			case(3):
				return "1/3 X a X b X c";
			case(4):
				return ("5 X a² X "+"\u221A9"+"3/4 X b");
			case(5):
				return ("6 X a² X "+"\u221A9"+"3/4 X b");
			case(6):
				return ("a X b X c");
			case(7):
				return ("a³");
		}
			return "";

	}

	private string getCaracteristicas() {

		switch(tipoForma){
		case(0):
			return "";
		case(1):
			return "Lado = " + sliderA.value + "\nAltura = " + sliderB.value;
		case(2):
			return "Lado = " + sliderA.value + "\nAltura = " + sliderB.value;
		case(3):
			return "Largura = " + sliderA.value + "\nAltura = " + sliderB.value + "\nComprimento = " + sliderC.value;
		case(4):
			return "Lado = " + sliderA.value + "\nAltura = " + sliderB.value;
		case(5):
			return "Lado = " + sliderA.value + "\nAltura = " + sliderB.value;
		case(6):
			return "Largura = " + sliderA.value + "\nAltura = " + sliderB.value + "\nComprimento = " + sliderC.value;
		case(7):
			return "Lado = " + sliderA.value;
		}
		return "";

	}

	private string titulos(int tipo) {

		switch(tipo){
		case(1):
			return "Piramide Base Triangular";
		case(2):
			return "Piramide Base Quadrada";
		case(3):
			return "Piramide Base Retangular";
		case(4):
			return "Prisma Base Pentagonal";
		case(5):
			return "Prisma Base Hexagonal";
		case(6):
			return "Paralelepiedo";
		case(7):
			return "Cubo";
		}
		return "";

	}


	public void MudaForma(int tipo){
		tipoForma = tipo;
		textArea1.text = textArea2.text = "Área: " + calculaArea();
		textVolume1.text = textVolume2.text = "Volume: " +calculaVolume();
		textAreaF1.text = textAreaF2.text = calculaTextoArea();
		textVolumeF1.text = textVolumeF2.text = calculaTextoVolume();
		titulo1.text = titulo2.text = titulos(tipo);

	}
}
