using UnityEngine;
using System.Collections;


public class Face :
	MonoBehaviour
{

	protected MeshFilter   face         = null;
	protected MeshRenderer renderizador = null;

	protected int numeroArestas = 0;


	public int getNumeroArestas() { return this.numeroArestas; }
	public int getNumeroTriangulos() { return getNumeroArestas() - 2; }

	public Vector3[] getVertices() { return this.face.mesh.vertices; }

	public Vector3 getCentro() { return this.transform.position; }

	public Vector3[] getArestaBase()
	{
		Vector3[] aresta = new Vector3[2];

		aresta [0] = getVertices () [0];
		aresta [1] = getVertices () [getNumeroArestas() - 1];

		return aresta;
	}


	public void setNumeroArestas(int numeroArestas)
	{
		if (numeroArestas >= 3)
		{
			this.numeroArestas = numeroArestas;

//			montarFace();
		}
	}


	protected virtual void montarFace()
	{
		Vector3[] vertices   = new Vector3[getNumeroArestas()];
		int[]     triangulos = new int[3 * getNumeroTriangulos()];


		Vector3 posicaoAtual = gameObject.transform.position;
		float
			anguloInterno = (2 * Mathf.PI) / getNumeroArestas(),
			anguloVertice = 0.0f;


		for (int t = 0; t < getNumeroTriangulos(); ++t)
		{
			triangulos[3 * t]     = 0;
			triangulos[3 * t + 1] = t + 1;
			triangulos[3 * t + 2] = t + 2;
		}


		for (int v = 0; v < getNumeroArestas(); ++v)
		{
			vertices[v].x = posicaoAtual.x + Mathf.Cos(anguloVertice);
			vertices[v].y = posicaoAtual.y + Mathf.Sin(anguloVertice);
			vertices[v].z = posicaoAtual.z;


			anguloVertice -= anguloInterno;
		}


		Mesh malha = gameObject.GetComponent<MeshFilter> ().mesh;

		malha.Clear();
		malha.vertices  = (Vector3[])vertices.Clone();
		malha.triangles = (int[])triangulos.Clone();
	}


	void Start ()
	{
		gameObject.AddComponent<MeshFilter>   ();
		gameObject.AddComponent<MeshRenderer> ();
		montarFace();
	}

	void Update ()
	{
	
	}
}

