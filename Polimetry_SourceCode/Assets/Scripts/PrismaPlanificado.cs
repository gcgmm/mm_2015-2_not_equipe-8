﻿using UnityEngine;
using System.Collections;

public class PrismaPlanificado :
	Poliedro
{

	public PrismaPlanificado() : base() { }

	public float getLado  () { return getMedidaA (); }
	public float getAltura() { return getMedidaB (); }


	override protected void montar()
	{

		setPlanificado(true);
		Vector3 posicaoCentral = gameObject.transform.position;
		int quantVerticesBaseTopo = getQuantVerticesBase ();
		int
			quantVerticesTotal = 4 * quantVerticesBaseTopo - 2;
		int
			idxInicioVertBase = 0,
			idxInicioVertTopo = quantVerticesTotal - quantVerticesBaseTopo;
			
		float
			deslocAltura    = getAltura () / 2.0f,
			anguloInterno   = getAnguloInternoRad (),
			raioExternoBase = getRaioExternoBase (getLado ()),
			raioInternoBase = getRaioInternoBase (getLado ());

		Vector3[] vertices = new Vector3[quantVerticesTotal];
		int[]     triangulos;


		//Vertices das faces base e topo.
		{
			int
				posVerticeBase = 0,
				posVerticeTopo = 0;
			//Iniciando com metade do angulo interno para que uma aresta
			// fique paralela ao eixo Y.
			float
				anguloVerticeBase = 1.5f * anguloInterno,            //1.5 * [Angulo Interno]
				anguloVerticeTopo = Mathf.PI - 0.5f * anguloInterno; //180° - ([Angulo Interno] / 2)

			for (int v = 0; v < quantVerticesBaseTopo; ++v)
			{
				posVerticeBase = idxInicioVertBase + v;
				posVerticeTopo = idxInicioVertTopo + v;

				//Face base
				vertices[posVerticeBase].x = posicaoCentral.x
					- deslocAltura
					- raioInternoBase
					+ raioExternoBase * Mathf.Cos(anguloVerticeBase);
				vertices[posVerticeBase].y = posicaoCentral.y;
				vertices[posVerticeBase].z = posicaoCentral.z
					+ raioExternoBase * Mathf.Sin(anguloVerticeBase);

				//Face topo
				vertices[posVerticeTopo].x = posicaoCentral.x
					+ deslocAltura
					+ raioInternoBase
					+ raioExternoBase * Mathf.Cos(anguloVerticeTopo);
				vertices[posVerticeTopo].y = posicaoCentral.y;
				vertices[posVerticeTopo].z = posicaoCentral.z
					+ raioExternoBase * Mathf.Sin(anguloVerticeTopo);


				anguloVerticeBase += anguloInterno;
				anguloVerticeTopo += anguloInterno;
			}
		}


		//Vertices das faces laterais.
		{
			int
				limiteIteracao           = quantVerticesBaseTopo - 1,
				idxInicioVertLateralBase = idxInicioVertBase + quantVerticesBaseTopo,
				idxInicioVertLateralTopo = idxInicioVertTopo - 1;
			float deslocLado = 1.5f * getLado();

			for (int v = 0; v < limiteIteracao; ++v)
			{
				vertices[idxInicioVertLateralBase + v].x = posicaoCentral.x
					- deslocAltura;
				vertices[idxInicioVertLateralBase + v].z = posicaoCentral.z
					+ deslocLado;
				vertices[idxInicioVertLateralBase + v].y = posicaoCentral.y;

				vertices[idxInicioVertLateralTopo - v].x = posicaoCentral.x
					+ deslocAltura;
				vertices[idxInicioVertLateralTopo - v].z = posicaoCentral.z
					+ deslocLado;
				vertices[idxInicioVertLateralTopo - v].y = posicaoCentral.y;

				deslocLado += getLado();
			}
		}


		{
			int[][] vertIndexesPerFace = new int[quantVerticesBaseTopo + 2][];
			int
				idxUltimoVertBase = idxInicioVertBase + quantVerticesBaseTopo - 1,
				idxUltimoVertTopo = idxInicioVertTopo + quantVerticesBaseTopo - 1;
			int
				idxInicioVertLateral = idxUltimoVertBase - 1,
				idxUltimoVertLateral = idxInicioVertTopo + 1;


			//Faces base e topo.
			vertIndexesPerFace[0] = new int[quantVerticesBaseTopo]; //Face base.
			vertIndexesPerFace[1] = new int[quantVerticesBaseTopo]; //Face topo.

			for (int v = 0; v < quantVerticesBaseTopo; ++v)
			{
				vertIndexesPerFace[0][v] = idxUltimoVertBase - v;
				vertIndexesPerFace[1][v] = idxUltimoVertTopo - v;
			}

			//Faces laterais.

			for (int f = 0; f < quantVerticesBaseTopo; ++f)
			{
				int posFace = 2 + f;

				vertIndexesPerFace[posFace] = new int[4];

				vertIndexesPerFace[posFace][1] =
					(vertIndexesPerFace[posFace][0] =
						idxInicioVertLateral + f)
					+ 1;
				vertIndexesPerFace[posFace][2] =
					(vertIndexesPerFace[posFace][3] =
					 	idxUltimoVertLateral - f)
					- 1;
			}

			triangulos = buildMesh (vertIndexesPerFace);
		}
		
		
		updateMesh (vertices, triangulos);
	}
	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro");
		ret.AddComponent<PrismaMontado>().quantVerticesBase = quantVerticesBase;
		return ret;
	}
}
