using UnityEngine;
using System.Collections;

public abstract class Poliedro :
	MonoBehaviour
{

	public int quantVerticesBase;

	public float
		medidaA,
		medidaB,
		medidaC;

	protected MeshFilter   meshFilter;
	protected MeshRenderer meshRenderer;
	protected bool planificado = false;


	public Poliedro()
	{
		this.quantVerticesBase = 3;
		this.medidaC = this.medidaB = this.medidaA = 1.0f;
	}


	public int getQuantVerticesBase() { return System.Math.Max(3, this.quantVerticesBase); }

	public void setPlanificado(bool value){
		this.planificado = value;
	}

	public float getMedidaA() { return this.medidaA; }
	public float getMedidaB() { return this.medidaB; }
	public float getMedidaC() { return this.medidaC; }


	protected MeshRenderer getMeshRenderer()
	{

		return GetComponent<MeshRenderer>();
	}

	protected MeshFilter getMeshFilter()
	{
		return GetComponent<MeshFilter>();
	}

	protected Mesh getMesh()
	{
		return getMeshFilter ().mesh;
	}


	public float getAnguloInternoRad() { return (2 * Mathf.PI) / getQuantVerticesBase(); }

	public float getRaioExternoBase(float ladoBase) { return ladoBase / (2 * Mathf.Sin (getAnguloInternoRad () / 2)); }
	public float getRaioInternoBase(float ladoBase) { return ladoBase / (2 * Mathf.Tan (getAnguloInternoRad () / 2)); }

	public float getApotema(float ladoBase, float alturaPoliedro)
	{
		float raioInternoBase = getRaioInternoBase (ladoBase);

		return
			Mathf.Sqrt (
					alturaPoliedro  * alturaPoliedro +
					raioInternoBase * raioInternoBase
				);
	}


	protected int[] buildMesh(int[][] vertexIndexes)
	{
		int[] quantTriangulosPorFace = new int[vertexIndexes.Length];
		int   quantTriangulosTotal   = 0;

		for (int f = 0; f < vertexIndexes.Length; ++f)
		{
			quantTriangulosTotal +=
				(quantTriangulosPorFace[f] =
				 	System.Math.Max(2, vertexIndexes[f].Length) - 2);
		}


		int[] triangVertIndexes = new int[3 * quantTriangulosTotal];
		int   curIdxTriangulos  = 0;

		for (int f = 0; f < vertexIndexes.Length; ++f)
		{
			for (int t = 0; t < quantTriangulosPorFace[f]; ++t)
			{
				triangVertIndexes[curIdxTriangulos++] = vertexIndexes[f][0];
				triangVertIndexes[curIdxTriangulos++] = vertexIndexes[f][t + 1];
				triangVertIndexes[curIdxTriangulos++] = vertexIndexes[f][t + 2];
			}
		}


		return triangVertIndexes;
	}


	virtual public void Start()
	{

		getMesh ().MarkDynamic ();
	}

	public void LateUpdate()
	{
		montar ();
	}

	protected void updateMesh(Vector3[] vertices, int[] triangulos)
	{
		Vector3[] correctedVerts  = new Vector3[triangulos.Length];
		int[]     correctedTriang = new int    [triangulos.Length];
		
		for(int i = 0; i < triangulos.Length; ++i)
		{
			correctedVerts [i] = vertices[triangulos[i]];
			correctedTriang[i] = i;
		}
		
		Mesh malha = getMesh ();
		
		malha.Clear ();
		malha.vertices  = correctedVerts;
		malha.triangles = correctedTriang;
		
		malha.RecalculateBounds  ();
		malha.RecalculateNormals ();
		malha.Optimize           ();
		
		Vector3 posAntiga = gameObject.transform.position;
		if (!this.planificado)
			posAntiga.y       = 0.25f * this.medidaB;
		else
			posAntiga.y       = 0.01f
				;
		gameObject.transform.position = posAntiga;
	}

	abstract protected void montar ();

	abstract public GameObject planificar();

}
