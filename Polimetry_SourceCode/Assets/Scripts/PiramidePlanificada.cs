﻿using UnityEngine;
using System.Collections;

public class PiramidePlanificada :
	Poliedro
{

	public PiramidePlanificada() : base() { }

	public float getLado   () { return getMedidaA (); }
	public float getAltura () { return getMedidaB (); }

	override protected void montar ()
	{
		setPlanificado(true);
		Vector3 posicaoCentral = gameObject.transform.position;
		
		int
			numVertBase  = getQuantVerticesBase (),
			numVertTotal = 2 * getQuantVerticesBase ();
		int
			idxInicioVertBase = 0,
			idxUltimoVertBase = numVertBase - 1,
			idxInicioVertTopo = numVertBase;
		float
			raioExternoBase = getRaioExternoBase (getLado ()),
			raioInternoBase = getRaioInternoBase (getLado ()),
			anguloInterno   = getAnguloInternoRad ();
		
		Vector3[] vertices = new Vector3[numVertTotal];

		//Face base.
		{
			//90° + ([Angulo Interno] / 2)
			float anguloVertice = (Mathf.PI / 2.0f) + 0.5f * anguloInterno;

			for (int v = 0; v < numVertBase; ++v)
			{
				vertices[v].x = posicaoCentral.x
					+ raioExternoBase * Mathf.Cos(anguloVertice);
				vertices[v].z = posicaoCentral.z
					+ raioExternoBase * Mathf.Sin(anguloVertice);
				vertices[v].y = posicaoCentral.y;


				anguloVertice += anguloInterno;
			}
		}


		//Faces laterais.
		{
			int posVertLateral = 0;
			float
				raioFaceLateral = raioInternoBase + getApotema (getLado (), getAltura ()),
				anguloVertice   = Mathf.PI / 2; //90°

			for (int v = 0; v < numVertBase; ++v)
			{
				posVertLateral = idxInicioVertTopo + v;

				vertices[posVertLateral].x = posicaoCentral.x
					+ raioFaceLateral * Mathf.Cos(anguloVertice);
				vertices[posVertLateral].z = posicaoCentral.z
					+ raioFaceLateral * Mathf.Sin(anguloVertice);
				vertices[posVertLateral].y = posicaoCentral.y;


				anguloVertice += anguloInterno;
			}
		}


		int[][] vertIndexesPerFace = new int[numVertBase + 1][];

		{
			//Face base.
			vertIndexesPerFace[0] = new int[numVertBase];

			for (int v = 0; v < numVertBase; ++v)
				vertIndexesPerFace[0][v] = idxUltimoVertBase - v;

			for (int f = 0; f < numVertBase; ++f)
			{
				int posFace = 1 + f;

				vertIndexesPerFace[posFace] = new int[3];

				vertIndexesPerFace[posFace][0] = idxInicioVertTopo + f;
				vertIndexesPerFace[posFace][2] =
					idxInicioVertBase +
					((vertIndexesPerFace[posFace][1] =
						  idxInicioVertBase +
						  (idxUltimoVertBase + f) % numVertBase)
					+ 1) % numVertBase;
			}
		}


		int[] triangulos = buildMesh (vertIndexesPerFace);

		updateMesh (vertices, triangulos);
	}

	override public GameObject planificar(){
		GameObject ret = new GameObject("Poliedro");
		ret.AddComponent<PiramideMontada>().quantVerticesBase = quantVerticesBase;
		return ret;
	}

}
