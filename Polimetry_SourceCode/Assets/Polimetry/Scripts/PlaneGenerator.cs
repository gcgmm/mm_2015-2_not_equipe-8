﻿using UnityEngine;
using System.Collections;

public class PlaneGenerator : MonoBehaviour {
	public Transform gridPrefab;
	public float linhas = 11;
	public float colunas = 11;
	private float novaLinha = 0;
	private float novaColuna = 0;
	// Use this for initialization
	void Start () {

		for (float l = 0; l < linhas; l++)
		{
			for (float c = 0; c < colunas; c++)
			{
				novaLinha = l-((linhas-1)/2);
				novaColuna = c-((colunas-1)/2);
				Instantiate (gridPrefab, new Vector3(novaLinha, transform.position.y, novaColuna), Quaternion.identity);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
