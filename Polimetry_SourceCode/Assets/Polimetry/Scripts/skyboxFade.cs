﻿using UnityEngine;
using System.Collections;

public class skyboxFade : MonoBehaviour {

	public ParticleSystem particles;
	private float lerp = 0;
	private float duration = 2;
	private float t = 1;
	private bool sky1 = true;
	private float tSky = 0;
	private float lerpRotation = 0;
	public GameObject planeGround;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		changeSky();
		rotateSky();
		keyboardControl();
	}

	void keyboardControl(){
		if(!sky1){
			iTween.MoveTo(planeGround,new Vector3(0,-10,0),2);
		}
	}

	void rotateSky(){
		tSky += Time.deltaTime/360f;
		lerpRotation = Mathf.Lerp(360.0f,0.0f,tSky);
		if(tSky >= 1)
			tSky = 0;
		RenderSettings.skybox.SetFloat("_Rotation",lerpRotation);
	}

    public void changeBoolSky() {
        sky1 = !sky1;
        t = 0;
    }


	void changeSky(){
		if (Input.GetKeyDown(KeyCode.F)){
			sky1 = !sky1;
			t = 0;
		}
		
		lerp = (sky1 ? Mathf.Lerp(0.0f,1.0f,t): Mathf.Lerp(1.0f,0.0f,t));
		if(t<1){
			t += Time.deltaTime/duration;
		}
		particles.startColor = new Color(lerp,lerp,lerp);
		RenderSettings.skybox.SetFloat("_SkyBlend", lerp);
	}
	void OnApplicationQuit(){
		RenderSettings.skybox.SetFloat("_SkyBlend",1f);
	}
}
