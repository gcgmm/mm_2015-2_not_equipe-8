﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class debug : MonoBehaviour {
	private GameObject[] canvasGroup;
	// Use this for initialization
	public Camera cam1;
	public Camera cam2;

	bool sky = true;

	private AudioSource music;

	void Start () {

		music = GetComponent<AudioSource>();
		music.mute = true;

		canvasGroup = GameObject.FindGameObjectsWithTag("CanvasMenu");
		foreach(GameObject canvasMenu in canvasGroup)
			canvasMenu.GetComponent<Canvas>().planeDistance = 0.31f;


	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.F)) {
			sky = !sky;
			foreach(GameObject canvasMenu in canvasGroup)
				canvasMenu.SetActive(sky);
		}

		if (Input.GetKeyDown(KeyCode.M))
			music.mute = !music.mute;
	}

}