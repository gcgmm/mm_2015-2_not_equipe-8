﻿using UnityEngine;
using System.Collections;

public class CursorControl : MonoBehaviour {

	private Vector3 mouse;
	private int w = 16;
	private int h = 16;
	public Texture2D cursor;
	public bool CursorVisivel = false;

	// Use this for initialization
	void Start () {
		UnityEngine.Cursor.visible = CursorVisivel;
	}
	
	// Update is called once per frame
	void Update () {
		UnityEngine.Cursor.visible = CursorVisivel;
		mouse = new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
	}

	void OnGUI(){
		GUI.DrawTexture(new Rect(mouse.x, mouse.y, w, h), cursor);
		//if(mouse.x >= (Screen.width/2))
			GUI.DrawTexture(new Rect((mouse.x + (Screen.width/2) )% (Screen.width), mouse.y, w, h), cursor);
	}
}
