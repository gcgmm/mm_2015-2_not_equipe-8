﻿using UnityEngine;
using System.Collections;

public class controlPlane : MonoBehaviour {

	public bool sky;
	private Vector3 originalpos, newposition;

	void Start () {
		originalpos = transform.position;
		newposition = transform.position;
		newposition.y = -100;
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.F))
		{
			sky = !sky;
			if(sky)
				iTween.MoveTo(gameObject,newposition,10);
			else
				iTween.MoveTo(gameObject,originalpos,2);
		}
	}
}