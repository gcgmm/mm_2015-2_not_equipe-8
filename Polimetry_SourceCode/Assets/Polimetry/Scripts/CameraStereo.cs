﻿using UnityEngine;
using System.Collections;

public class CameraStereo : MonoBehaviour {
	public Transform target;
	public float displacement=3;
	public Camera cameraL;

	public float distance = 10;
	public float cameraSpeed = 20;
	public float xSpeed = 175;
	public float ySpeed = 75;
    private bool sky = false;

	float yMinLimit = 5; //Lowest vertical angle in respect with the target.
	float yMaxLimit = 80;
	float minDistance = 5; //Min distance of the camera from the target
	float maxDistance = 20;
	float x = 0.0f;
	float y = 0.0f;

	void Start () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		keyBoardControl();
		smoothCamera();
		//simpleStereo();
	}

	void smoothCamera(){

		if (target) {
			
			//Zooming with mouse
			distance += Input.GetAxis("Mouse ScrollWheel")*distance;
			distance = Mathf.Clamp(distance, minDistance, maxDistance);
			
			//Detect mouse drag;
			if(Input.GetMouseButton(1)) {
				
				x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
				y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;        
			}
			if (Input.GetKey(KeyCode.A))
				x += xSpeed * 0.02f;
			if (Input.GetKey(KeyCode.D))
				x -= xSpeed * 0.02f;
			if (Input.GetKey(KeyCode.W))
				y += xSpeed * 0.02f;
			if (Input.GetKey(KeyCode.S))
				y -= xSpeed * 0.02f;
			y = ClampAngle(y, yMinLimit, yMaxLimit);


			Quaternion rotation = Quaternion.Euler(y, x, 0);
			Vector3 position = rotation * (new Vector3(0.0f, 0.0f, -distance)) + target.position;
			//When the target changed or moved, Move the camera smoothly with it.       
			transform.position = Vector3.Lerp (transform.position, position, cameraSpeed*Time.deltaTime);
			transform.rotation = rotation;
			cameraL.transform.position = transform.position;
			transform.rotation = transform.rotation;

			cameraL.transform.RotateAround(target.position, Vector3.up, (displacement*2));
			cameraL.transform.LookAt(target);
			transform.LookAt(target);
			
		}
	}

    public void changeSkyBool() {
        sky = !sky;
        yMinLimit = (sky ? -80 : 5);
    }

	void keyBoardControl(){
		if (Input.GetKey("f1") && displacement>-200) 
			displacement-=0.01f;
		if (Input.GetKey("f2") && displacement<200) 
			displacement+=0.01f;
        if (Input.GetKeyDown("f")){
            sky = !sky;
            yMinLimit = (sky ? -80 : 5);

        }
        if (Input.GetKeyDown("escape"))
            Application.Quit();
		if (Input.GetKeyUp(KeyCode.X)){
			Camera c=transform.GetComponent<Camera>();
			if(c.rect.width==0.5f)
				c.rect=new Rect(0,0,1,1);
			else
				c.rect=new Rect(0,0,0.5f,1);
		}

	}
	private static float ClampAngle (float angle,float min,float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}

}
